// função construtora da instância do Vue
new Vue({
  el: '#app',
  data: {
    titulo: 'Usando VueJS!'
  },
  methods: {
    saudacao() {
      return 'Boa semana!'
    }
  }
})