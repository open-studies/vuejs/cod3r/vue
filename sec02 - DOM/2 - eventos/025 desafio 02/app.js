new Vue({
    el: '#desafio',
    data: {
        valor: ''
    },
    methods: {
        exibeAlerta() {
            alert("O botão foi clicado!");
        },
        leituraInput(ev) {
            this.valor = ev.target.value;
        }
    }
})