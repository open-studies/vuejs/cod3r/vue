new Vue({
	el: '#desafio',
	data: {
		// 1
		boolean1: true,
		// 2
		array: ['Pedro', 'Bia', 'Ana', 'Rebeca'],
		// 3
		livro: {
			titulo: 'O Senhor dos Anéis',
			autor: 'J.R.R. Tolkiens',
			volume: '3'
		},
		// 4
		aluno: {
			id: 10,
			nome: 'Maria',
			notas: [7.67, 8.33, 6.98, 9.21]
		}
	}
});
