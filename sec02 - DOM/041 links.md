# Links - Documentação oficial

## 1. Introdução
- [Original](https://vuejs.org/v2/guide/index.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/)

## 2. Sintaxe de Templates
- [Original](https://vuejs.org/v2/guide/syntax.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/syntax.html)

## 3. Manipulação de Eventos
- [Original](https://vuejs.org/v2/guide/events.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/events.html)

## 4. Dados Computados e Observadores
- [Original](https://vuejs.org/v2/guide/computed.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/computed.html)

## 5. Interligações de Classe e Estilos
- [Original](https://vuejs.org/v2/guide/class-and-style.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/class-and-style.html)