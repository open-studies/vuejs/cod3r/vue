new Vue({
  el: '#app',
  data: {
    contador: 0,
    x: 0,
    y: 0
  },
  methods: {
    // mesmo que o evento não seja recebido como parâmetro, ele é passado por padrão
    somar(evt) {
      console.log(evt);
      this.contador++
    },
    atualizarXY(e) {
      this.x = e.clientX;
      this.y = e.clientY;
    }
  }
})