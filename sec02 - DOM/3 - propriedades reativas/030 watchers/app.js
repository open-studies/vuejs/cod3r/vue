new Vue({
  el: '#app',
  data: {
    contador1: 0,
    contador2: 0,
    zeracoes1: 0,
    zeracoes2: 0
  },
  computed: {
    resultado1() {
      console.log('... computed property - resultado1 ...');
      return this.contador1 >= 5 ?
        'Maior ou igual a 5' :
        'Menor que 5'
    },
    resultado2() {
      console.log('... computed property - resultado2 ...');
      return this.contador2 >= 5 ?
        'Maior ou igual a 5' :
        'Menor que 5'
    }
  },
  watch: {
    // não deve haver conflito de nomes de variáveis e de métodos nas propriedades (objetos) 'data', 'computed' e 'methods'. mas em 'watch', deve-se indicar o nome do dado ou computed property que se deseja monitorar e, usando esse nome, criar aqui uma função de mesmo nome.
    // outro exemplo de uso: 2 comboxes, sendo um pra UF e outro pra cidade. ao alterar o valor do 1o (de UF), com um watcher nele, alterar os itens do 2o por requisição ajax.
    /*
      tomar cuidado em casos quando:
      - uma alteração disparar uma mudança,
      - que pode disparar um watch,
      - que pode gerar uma mudança
      - e assim por diante, infinitamente.
      Deve-se ter cuidado com esses casos (circulares?)
      ver melhor isso depois!
      Vide vídeo a033, em 02m41s
    */
    contador1(novo, antigo) {
      console.log(`=== começo watcher contador 1 ===`);
      console.log(`>>> o valor de contador1 foi alterado <<<`)
      console.log(`contador1 - valor novo: ${novo}`);
      console.log(`contador1 - valor antigo: ${antigo}`);
      setTimeout(() => {
        // esse vídeo tem uma explicação sobre a efetividade de uso do this em arrow function e this léxico
        console.log(`--- início setTimeout do contador1 ---`)
        this.contador1 = 0;
        this.zeracoes1++;
        console.log(`zerações1: ${this.zeracoes1}`);
        console.log(`--- fim setTimeout do contador1 ---`)
      }, 2000);
      console.log(`zerações1: ${this.zeracoes1}`);
      console.log(`=== fim watcher contador 1 ===`)
    },
    contador2(novo, antigo) {
      console.log(`=== começo watcher contador 2 ===`);
      console.log(`>>> o valor de contador2 foi alterado <<<`)
      console.log(`contador2 - valor novo: ${novo}`);
      console.log(`contador2 - valor antigo: ${antigo}`);
      setTimeout(() => {
        console.log(`--- início setTimeout do contador2 ---`)
        this.contador2 = 0;
        this.zeracoes2++;
        console.log(`zerações2: ${this.zeracoes2}`);
        console.log(`--- fim setTimeout do contador2 ---`)
      }, 2000);
      console.log(`zerações2: ${this.zeracoes2}`);
      console.log(`=== fim watcher contador 2 ===`)
    }
  },
  methods: {
    aumentarCont1() {
      this.contador1++
    },
    aumentarCont2() {
      this.contador2++
    },
    diminuirCont1() {
      this.contador1--
    },
    diminuirCont2() {
      this.contador2--
    }
  }
})