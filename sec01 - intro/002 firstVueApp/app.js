// não é necessário criar uma constante, para nela instanciar o Vue, como:
// const v = new Vue({});
new Vue({
  el: '#app',
  data: {
    titulo: 'Usando VueJS 2!'
  }
})