new Vue({
  el: '#app',
  data: {
    titulo: 'Usando VueJS!'
  },
  methods: {
    saudacao() {
      // dentro da instância Vue, consegue-se acessar dados do objeto interno 'data', sem necessidade de acessar elemento-pai, fazendo uso apenas de 'this'.
      // outras funções também podem ser acessadas pelo 'this'
      // os atributos (dados) do objeto interno 'data' e as funções no objeto interno 'methods' são colocados na raíz da instância Vue. dessa forma, o acesso pelo 'this' é possível. consequência: data e methods não podem conter dados e funções com nomes iguais, pois haverá conflitos.
      console.log(this);
      return this.titulo
    }
  }
})