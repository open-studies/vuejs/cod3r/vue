new Vue({
  el: '#app',
  data: {
    titulo: 'Usando VueJS!',
    link: 'https://google.com',
    // essa não é uma boa prática de segurança (xss attack), e o Vue a previne, não interpretando o texto que será interpolado como HTML de fato.
    linkHtml: '<a href="https://google.com">Google</a>'
  },
  methods: {
    saudacao() {
      this.titulo = 'Bom dia!!!'
      return this.titulo
    }
  }
})