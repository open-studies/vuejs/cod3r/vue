new Vue({
  el: '#app',
  data: {
    cor: 'red',
    largura: 100,
    altura: 100
  },
  computed: {
    // para estilizações mais complexas, é mais interessante criar prop computada, retornando conforme a lógica especificada
    estiloCaixa2() {
      return {
        backgroundColor: this.cor,
        width: `${this.largura}px`,
        height: `${this.altura}px`
      }
    }
  }
});