new Vue({
  el: '#app',
  data: {
    contador: 0,
    x: 0,
    y: 0
  },
  methods: {
    somar(passo, event) {
      console.log(passo);
      console.log(event);
      this.contador += passo
    },
    atualizarXY(e) {
      this.x = e.clientX;
      this.y = e.clientY;
    }
  }
})