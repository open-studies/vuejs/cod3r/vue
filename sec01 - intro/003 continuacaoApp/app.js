new Vue({
  el: '#app',
  data: {
    titulo: 'Usando VueJS 2!'
  },
  // métodos executados, por exemplo, com eventos html
  methods: {
    alterarTitulo(evt) {
      this.titulo = evt.target.value
    }
  }
})