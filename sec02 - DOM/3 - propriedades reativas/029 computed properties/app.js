new Vue({
  el: '#app',
  data: {
    contador1: 0,
    contador2: 0
  },
  computed: {
    resultado1() {
      console.log('computed property - resultado1');
      return this.contador1 >= 5 ?
        'Maior ou igual a 5' :
        'Menor que 5'
    },
    resultado2() {
      console.log('computed property - resultado2');
      return this.contador2 >= 5 ?
        'Maior ou igual a 5' :
        'Menor que 5'
    }
  },
  methods: {
    aumentarCont1() {
      this.contador1++
    },
    aumentarCont2() {
      this.contador2++
    },
    diminuirCont1() {
      this.contador1--
    },
    diminuirCont2() {
      this.contador2--
    },
    // resultado1() {
    //   console.log('método - resultado1');
    //   return this.contador1 >= 5 ?
    //     'Maior ou igual a 5' :
    //     'Menor que 5'
    // },
    // resultado2() {
    //   console.log('método - resultado2');
    //   return this.contador2 >= 5 ?
    //     'Maior ou igual a 5' :
    //     'Menor que 5'
    // }
  }
})