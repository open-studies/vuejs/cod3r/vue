new Vue({
  el: '#app',
  data: {
    aplicarC1: false,
    aplicarC2: false,
    aplicarC3: false,
    aplicarGirar: false,
    classeCaixa3: 'c1'
  },
  computed: {
    estilo1() {
      return {
        c1: this.aplicarC1,
        c2: !this.aplicarC1
      }
    }
  }
});