# Links - Documentação oficial

## 1. Renderização condicional
- [Original](https://vuejs.org/v2/guide/conditional.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/conditional.html)

## 2. Renderização de Listas
- [Original](https://vuejs.org/v2/guide/list.html)
- [Tradução pt-br](https://br.vuejs.org/v2/guide/list.html)
