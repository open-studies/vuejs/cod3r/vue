new Vue({
	el: '#desafio',
	data: {
		// 01
		classe1: 'destaque',
		// 03
		classe3: '',
		// 04
		classe4: '',
		sucesso4: '',
		// 05
		marginTop5: '',
		cor5: '',
		largura5: '',
		altura5: '',
		// 06
		width6: '0%',
		temporizador: ''
	},
	methods: {
		// 01
		iniciarEfeito() {
			setInterval(() => {
				this.classe1 = this.classe1 == 'destaque' ? 'encolher' : 'destaque'
			}, 1000);
		},
		// 04
		setSucesso4(e) {
			if (e.target.value == 'true') this.sucesso4 = true;
			if (e.target.value == 'false') this.sucesso4 = false;
		},
		// 06
		handler6(callback1, callback2) {
			this[callback1()];
			this[callback2()];
		},
		reiniciarProgresso() {
			clearInterval(this.temporizador);
			this.width6 = '0%';
		},
		iniciarProgresso() {
			let progresso = 0;
			this.temporizador = setInterval(() => {
				progresso += 5;
				this.width6 = `${progresso}%`
				if (progresso == 100) clearInterval(this.temporizador);
				console.log('exec_in');
			}, 500);
		}
	}
})
